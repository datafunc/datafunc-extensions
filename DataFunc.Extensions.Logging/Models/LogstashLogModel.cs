﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.CompilerServices;
using System.Text;
using Newtonsoft.Json;

namespace DataFunc.Extensions.Logging.Models
{
    public class LogstashLogModel
    {
    

        [JsonProperty("_index")]
        public string Index { get; set; }
        
        [JsonProperty("level")]
        public string Level { get; set; }
        [JsonProperty("messageTemplate")]
        public string MessageTemplate { get; set; }
        public object Message { get; set; }
        public string FileName { get; set; }
        public IEnumerable<KeyValuePair<string,object>> LoggingValues { get; set; }

        public string Serialize()
        {
            var sb = new StringBuilder();
            sb.AppendLine(JsonConvert.SerializeObject(new { index = new LogStashHeaderLogModel { Index = this.Index } }).Replace("{{", "{").Replace("}}", "}"));
            sb.AppendLine(JsonConvert.SerializeObject(BuildLoggingObject()).Replace("replacemetimestamp","@timestamp"));
            return sb.ToString();
        }

        private object BuildLoggingObject()
        {
            dynamic logging = new ExpandoObject();
            logging.replacemetimestamp = DateTime.Now;
            logging.level = this.Level;
            logging.messageTemplate = MessageTemplate;
            logging.message = JsonConvert.SerializeObject(Message, Formatting.Indented);

            if (!string.IsNullOrWhiteSpace(FileName))
                logging.file = FileName;

            if (LoggingValues != null)
            {
                var parsedExpandoObject = logging as IDictionary<string, Object>;
                foreach (var loggingValue in LoggingValues)
                {
                    parsedExpandoObject.Add(loggingValue.Key, JsonConvert.SerializeObject(loggingValue.Value));
                }
            }
         

            return logging;

        }

    [JsonObject("index")]
    private class LogStashHeaderLogModel
    {
        [JsonProperty("_index")]
        public string Index { get; set; }


        [JsonProperty("_type")]
        public string Type => "_doc";
    }
    }


}