﻿namespace DataFunc.Extensions.Logging.Configuration
{
    public class DataFuncLoggingOptions
    {
        public string LogStreamPrefix { get; set; }
        public string LogGroup { get; set; }
    }
}