﻿using System;
using AWS.Logger;
using DataFunc.Extensions.Logging.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DataFunc.Extensions.Logging
{
    public static class DataFuncLogExtensions
    {
        public static ILoggingBuilder UseDataFuncLogging(this ILoggingBuilder builder, DataFuncLoggingOptions options)
        {
            return builder.AddAWSProvider(new AWSLoggerConfig(options.LogGroup)
            {
                LogStreamNamePrefix = options.LogStreamPrefix,
                Region = "eu-west-1",
                DisableLogGroupCreation = true
            }, ShouldLog());
        }

        private static Func<string, LogLevel, bool> ShouldLog()
        {
            return (assembly, level) => !assembly.StartsWith("microsoft", StringComparison.CurrentCultureIgnoreCase) || level > LogLevel.Information;
        }
        public static void UseDataFuncLogging(this IServiceCollection builder, DataFuncLoggingOptions options)
        {
            builder.AddLogging(x => x.UseDataFuncLogging(options));
        }
        public static void UseDataFuncLogging(this IServiceCollection builder, IConfigurationSection section)
        {
            builder.AddLogging(x => x.UseDataFuncLogging(section.Get<DataFuncLoggingOptions>()));
        }



    }
}