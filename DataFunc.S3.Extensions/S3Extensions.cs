﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon.S3;
using Amazon.S3.Model;
using DataFunc.S3.Extensions.Models;

namespace DataFunc.S3.Extensions
{
    public static class S3Extensions
    {

        private static async Task<GetObjectResponse> GetObjectResponse(this IAmazonS3 client, string folder, string fileName, CancellationToken token)
        {
            return await client.GetObjectAsync(new GetObjectRequest {
                BucketName = folder,
                Key = fileName,
            }, token);
        }
        public static async Task<bool> Exists(this IAmazonS3 client, string folder, string fileName, CancellationToken token)
        {
            try
            {
                var getObjectResponse = await GetObjectResponse(client, folder, fileName, token);
                return getObjectResponse.HttpStatusCode == HttpStatusCode.OK;
            }
            catch (AmazonS3Exception e)
            {
                return e.StatusCode == HttpStatusCode.OK;
            }
        }
        public static async Task<string> UploadFromRemoteUrl(this IAmazonS3 client, string folder, string name, string remoteUrl, CancellationToken cancellationToken, bool isPublic, params KeyValuePair<string, string>[] properties)
        {
            using (var http = new HttpClient())
            {
                var fileContents = await http.GetByteArrayAsync(remoteUrl);
                return await Upload(client, folder, name, fileContents, isPublic, cancellationToken, properties);
            }
        }
        public static Task<string> Upload(this IAmazonS3 client, string folder, string name, byte[] fileContents, bool isPublic, CancellationToken cancellationToken, params KeyValuePair<string, string>[] additionalProperties)
        {
            return Upload(client, folder, name, new MemoryStream(fileContents), cancellationToken, isPublic, additionalProperties);
        }
        public static Task<string> Upload(this IAmazonS3 client, string folder, string name, string fileContents, bool isPublic, CancellationToken cancellationToken, params KeyValuePair<string, string>[] additionalProperties)
        {
            return Upload(client, folder, name, new MemoryStream(Encoding.UTF8.GetBytes(fileContents)), cancellationToken, isPublic, additionalProperties);
        }
        public static async Task<string> Upload(this IAmazonS3 client, string folder, string name, Stream stream, CancellationToken cancellationToken, bool isPublic, params KeyValuePair<string, string>[] properties)
        {
            using (var toDisposeStream = stream)
            {
                var putObjectRequest = new PutObjectRequest {
                    BucketName = folder,
                    Key = name,
                    InputStream = toDisposeStream,

                    CannedACL = isPublic ? S3CannedACL.PublicRead : S3CannedACL.Private
                };

                if (properties != null && properties.Any())
                {
                    foreach (var keyValuePair in properties)
                    {
                        putObjectRequest.Metadata.Add(keyValuePair.Key, keyValuePair.Value);
                    }
                }

                await client.PutObjectAsync(putObjectRequest, cancellationToken);
                return GetUrl(client, folder, name);
            }
        }
        public static string GetUrl(this IAmazonS3 s3Client, string bucket, string key)
        {
            return "https://" + bucket + "." + s3Client.Config.RegionEndpointServiceName + "-" + s3Client.Config.RegionEndpoint.SystemName + ".amazonaws.com/" + key;
        }
        public static Task<string> GenerateDownloadUrl(this IAmazonS3 s3Client, string folder, string name, int expirationInMinutes = 15)
        {
            return Task.FromResult(s3Client.GeneratePreSignedURL(folder, name, DateTime.Now.AddMinutes(expirationInMinutes), null));
        }
        public static Task Copy(this IAmazonS3 client, string sourceFolder, string sourceName, string destinationFolder, string destinationName, CancellationToken cancellationToken)
        {
            return client.CopyObjectAsync(sourceFolder, sourceName, destinationFolder, destinationName, cancellationToken);
        }
        public static async Task Move(this IAmazonS3 client, string sourceFolder, string sourceName, string destinationFolder, string destinationName, CancellationToken cancellationToken)
        {
            await Copy(client, sourceFolder, sourceName, destinationFolder, destinationName, cancellationToken);
            await client.DeleteObjectAsync(sourceName, sourceName, cancellationToken);
        }
        public static async Task<IEnumerable<string>> ReadAllLinesFromFile(this IAmazonS3 s3Client, string folder, string name, CancellationToken token)
        {
            var results = new List<string>();

            using (var downloadStream = await DownloadToStream(s3Client, folder, name, token))
            using (var streamReader = new StreamReader(downloadStream))
            {
                while (!streamReader.EndOfStream)
                {
                    results.Add(await streamReader.ReadLineAsync());
                }
            }
            return results.AsEnumerable();
        }
        public static async Task<string> ReadAllTextFromFile(this IAmazonS3 s3Client, string folder, string name, CancellationToken token)
        {
            using (var downloadStream = await DownloadToStream(s3Client, folder, name, token))
            using (var streamReader = new StreamReader(downloadStream))
            {
                return await streamReader.ReadToEndAsync();
            }
        }
        public static async Task<Stream> DownloadToStream(this IAmazonS3 s3Client, string folder, string name, CancellationToken token)
        {
            var response = await GetObjectResponse(s3Client, folder, name, token);
            return response.ResponseStream;
        }
        public static async Task DeleteUnversionedbjects(this IAmazonS3 s3Client, string folder, CancellationToken token, params string[] toDeleteKeys)
        {
            var deleteObjectRequest = new DeleteObjectsRequest {
                BucketName = folder,
                Objects = toDeleteKeys.Select(x => new KeyVersion {
                    Key = x
                }).ToList()
            };

            await s3Client.DeleteObjectsAsync(deleteObjectRequest, token);
        }
        public static async Task<Dictionary<string, Dictionary<string, string>>> GetTagsFromObjects(this IAmazonS3 client, string folder, CancellationToken token, params string[] keys)
        {
            var result = new Dictionary<string, Dictionary<string, string>>();
            foreach (var key in keys)
            {
                result[key] = await GetTagsFromObject(client, folder, key, token);
            }

            return result;
        }
        public static async Task<Dictionary<string, string>> GetTagsFromObject(this IAmazonS3 client, string folder, string key, CancellationToken token)
        {
            var request = new GetObjectTaggingRequest {
                BucketName = folder,
                Key = key
            };

            var response = await client.GetObjectTaggingAsync(request, token);
            return response.Tagging.ToDictionary(x => x.Key, x => x.Value);
        }
        public static async Task<IEnumerable<S3Object>> GetObjectsByPrefix(this IAmazonS3 client, string folder, string prefix, CancellationToken token)
        {
            var request = new ListObjectsV2Request {
                BucketName = folder,
                MaxKeys = 1000,
                Prefix = prefix
            };

            var results = new List<S3Object>();

            do
            {
                var response = await client.ListObjectsV2Async(request, token);
                request.ContinuationToken = response.ContinuationToken;
                results.AddRange(response.S3Objects);
            } while (!string.IsNullOrWhiteSpace(request.ContinuationToken));

            return results;
        }
        public static async Task<byte[]> DownloadToByteArray(this IAmazonS3 s3Client, string folder, string name, CancellationToken token)
        {
            using (var ms = new MemoryStream())
            using (var stream = await DownloadToStream(s3Client, folder, name, token))
            {
                await stream.CopyToAsync(ms);
                return ms.ToArray();
            }
        }


        public static async Task UpdateTags(this IAmazonS3 client, string folder, string key, Dictionary<string, string> tags, TagUpdateMethod method, CancellationToken token)
        {
            var existingTags = method == TagUpdateMethod.Merge ? await GetTagsFromObject(client, folder, key, token) : new Dictionary<string, string>();

            foreach (var tag in tags)
            {
                existingTags[tag.Key] = tag.Value;
            }

            await client.PutObjectTaggingAsync(new PutObjectTaggingRequest {
                Key = key,
                BucketName = folder,
                Tagging = new Tagging {
                    TagSet = existingTags.Select(x => new Tag { Key = x.Key, Value = x.Value }).ToList()
                }
            }, token);
        }
    }
}
