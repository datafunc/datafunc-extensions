﻿namespace DataFunc.S3.Extensions.Models
{
    public enum TagUpdateMethod
    {
        ReplaceAll,
        Merge
    }
}