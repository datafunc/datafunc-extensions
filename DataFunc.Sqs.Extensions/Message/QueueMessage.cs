﻿namespace DataFunc.Sqs.Extensions.Message
{
    public class QueueMessage<T>
    {
        public string MessageId { get; set; }
        public string ReceiptHandle { get; set; }
        public T Contents { get; set; }
        public string OriginalBody { get; set; }
    }
}