﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.SQS;
using Amazon.SQS.Model;
using DataFunc.Extensions;
using DataFunc.Sqs.Extensions.Message;
using Newtonsoft.Json;

namespace DataFunc.Sqs.Extensions
{
    public static class SqsExtensions
    {
        public static async Task SendMessages(this IAmazonSQS service, string queueUrl, IEnumerable<object> messages,
            int visibilityTimeOutInSeconds = 0, CancellationToken token = default)
        {

            foreach (var batch in messages.Batch(10))
            {
                await service.SendMessageBatchAsync(queueUrl, batch.Select(x =>
                        new SendMessageBatchRequestEntry(
                            Guid.NewGuid().ToString(),
                            SerializeWithDefaultSettings(x))
                        {
                            DelaySeconds = visibilityTimeOutInSeconds
                        })
                    .ToList(), token);
            }
        }

        public static Task SendMessages(this IAmazonSQS service, string queueUrl, IEnumerable<object> messages,
            CancellationToken token = default)
        {
            return SendMessages(service, queueUrl, messages, 0, token);
        }

        public static Task SendSingleMessage(this IAmazonSQS service, string queueUrl, object message,
            int visibilityTimeOutInSeconds = 0, CancellationToken token = default)
        {
            var putMessageRequest = new SendMessageRequest(queueUrl, SerializeWithDefaultSettings(message))
            {
                DelaySeconds = visibilityTimeOutInSeconds
            };


            return service.SendMessageAsync(putMessageRequest, token);
        }

        public static Task SendSingleMessage(this IAmazonSQS service, string queueUrl, object message,
            CancellationToken token = default)
        {
            return SendSingleMessage(service, queueUrl, message, 0, token);
        }

        public static Task<IEnumerable<QueueMessage<T>>> ReceiveMessages<T>(this IAmazonSQS service, string queueUrl, CancellationToken token = default)
        {
            return ReceiveMessages<T>(service, queueUrl, 20, token);
        }

        public static async Task<IEnumerable<QueueMessage<T>>> ReceiveMessages<T>(this IAmazonSQS service,
            string queueUrl, int waitTimeSeconds, CancellationToken token = default)
        {
            var receiveMessageRequest = new ReceiveMessageRequest(queueUrl)
            {
                MaxNumberOfMessages = 10,
                WaitTimeSeconds = waitTimeSeconds
            };

            var response = await service.ReceiveMessageAsync(receiveMessageRequest, token);
            return response.Messages.Select(x => new QueueMessage<T>
            {
                MessageId = x.MessageId,
                OriginalBody = x.Body,
                Contents = JsonConvert.DeserializeObject<T>(x.Body),
                ReceiptHandle = x.ReceiptHandle
            });
        }


        private static string SerializeWithDefaultSettings(object toDeserializeObject)
        {
            return JsonConvert.SerializeObject(toDeserializeObject, new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All,
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore,
                Formatting = Formatting.Indented,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }
    }
}
