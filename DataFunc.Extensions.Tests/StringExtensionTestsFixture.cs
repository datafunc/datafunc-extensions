using NUnit.Framework;

namespace DataFunc.Extensions.Tests
{
    [TestFixture]
    public class StringExtensionTestsFixture
    {
        [Test]
        [TestCase("MyWeirdlyCasedString", "myweirdlycasedstring", true)]
        [TestCase("myweirdlylowercasedstring", "myweirdlylowercasedstring", true)]
        [TestCase("MYUPPERCASESTRING", "myupperCaseString", true)]
        [TestCase("myfirstcasestring", "mysecondcasestring", false)]
        public void EnsureEqualsIgnoreCaseCompares(string inputString, string expectedString, bool shouldMatch)
        {
            Assert.That(inputString.EqualsIgnoreCase(expectedString) == shouldMatch);
        }


        [Test]
        [TestCase("Abcd123@pet/*-", "Abcdpet", true)]
        [TestCase("Abcd123@pet/*-", "Abcd123@pet/*-", false)]
        [TestCase("@@@HELLO�_WORLD", "HELLOWORLD", true)]
        [TestCase("@@@HELLO�_WORLD", "@@@HELLO�_WORLD", false)]
        public void EnsureExtractLettersOnlyContainsLetters(string inputString, string expectedString, bool shouldMatch)
        {
            var isMatch = inputString.ExtractLetters() == expectedString;
            Assert.AreEqual(shouldMatch, isMatch);
        }

        [Test]
        [TestCase("Abcd123@pet/*-", "123", true)]
        [TestCase("Abcd123@pet/*-", "Abcd13@pet/*-", false)]
        [TestCase("@@1@HE1LLO�_WOR1LD", "111", true)]
        [TestCase("@@@HELLO�12_WORLD", "@@@HEL1LO�_WORLD", false)]
        public void EnsureExtractNumbersOnlyContainsNumbers(string inputString, string expectedString, bool shouldMatch)
        {
            if (shouldMatch)
            {
                Assert.AreEqual(expectedString, inputString.ExtractNumbers());
            }
            else
            {
                Assert.AreNotEqual(expectedString, inputString.ExtractNumbers());
            }
        }

        [Test]
        [TestCase("Abcd123@pet/*-", "Abcd@pet/*-", true)]
        [TestCase("Abcd123@pet/*-", "Abcd13@pet/*-", false)]
        [TestCase("@@1@HE1LLO�_WOR1LD", "@@@HELLO�_WORLD", true)]
        [TestCase("@@@HELLO�12_WORLD", "@@@HEL1LO�_WORLD", false)]
        public void EnsureRemoveNumbersRemovesNumbers(string inputString, string expectedString, bool shouldMatch)
        {
            if (shouldMatch)
            {
                Assert.AreEqual(expectedString, inputString.RemoveNumbers());
            }
            else
            {
                Assert.AreNotEqual(expectedString, inputString.RemoveNumbers());
            }
        }

        [Test]
        [TestCase("Abcd123@pet/*-", 6, "Abc...")]
        [TestCase("123456@pet/*-", 8, "12345...")]
        public void EnsureTruncateDotsAreIncluded(string inputString, int toTruncateLength, string expectedResult)
        {
            Assert.AreEqual(expectedResult, inputString.Truncate(toTruncateLength));
        }
    }
}
