﻿using System;
using System.Globalization;
using NUnit.Framework;

namespace DataFunc.Extensions.Tests
{
    [TestFixture]
    public class DateTimeExtensionTests
    {
        private DateTime ParseDutchDate(string inputString)
        {
            return DateTime.ParseExact(inputString, "dd/MM/yyyy", new CultureInfo("nl-BE"), DateTimeStyles.None);
        }

        private string ToDutchDate(DateTime inputDate)
        {
            return inputDate.ToString("dd/MM/yyyy");
        }

        [Test]
        [TestCase("22/07/2019","22/07/2019")]
        [TestCase("24/07/2019", "22/07/2019")]
        [TestCase("26/07/2019", "22/07/2019")]
        [TestCase("28/07/2019", "22/07/2019")]
        [TestCase("21/07/2019", "15/07/2019")]
        public void EnsureStartOfWeekMatches(string startDate, string expectedDate)
        {
            Assert.AreEqual(ParseDutchDate(expectedDate), ParseDutchDate(startDate).StartOfWeek());
        }

        [Test]
        [TestCase("22/07/2019", "28/07/2019")]
        [TestCase("24/07/2019", "28/07/2019")]
        [TestCase("26/07/2019", "28/07/2019")]
        [TestCase("28/07/2019", "28/07/2019")]
        [TestCase("15/07/2019", "21/07/2019")]
        public void EnsureEndOfWeekMatches(string startDate, string expectedDate)
        {
            Assert.AreEqual(ParseDutchDate(expectedDate), ParseDutchDate(startDate).EndOfWeek());
        }
    }
}