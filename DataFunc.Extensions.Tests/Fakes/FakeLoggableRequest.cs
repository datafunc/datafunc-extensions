﻿using System;
using MediatR;

namespace DataFunc.Extensions.Tests.Fakes
{
    public class FakeLoggableRequest : IRequest
    {
        public int FakePropertyOne => new Random().Next(0, int.MaxValue);
        public int FakePropertyTwo => new Random().Next(0, int.MaxValue);
        public int FakePropertyThree => new Random().Next(0, int.MaxValue);
    }
}