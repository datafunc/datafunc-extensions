﻿using System;
using DataFunc.Extensions.MediatR.Extensions;
using DataFunc.Extensions.MediatR.Interfaces;
using MediatR;

namespace DataFunc.Extensions.Tests.Fakes
{
    public class SecondFakeCachableRequestWithoutProperties : IRequest<Guid>, ICachable
    {
        public string CacheKey => this.GenerateCacheKey(nameof(ICachable));
    }
}