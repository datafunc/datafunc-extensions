﻿using DataFunc.Extensions.MediatR.Interfaces;

namespace DataFunc.Extensions.Tests.Fakes
{
    public class FakeClearCacheCommand : ICanClearCache
    {
        public string CachePrefix => nameof(ICachable);
    }
}