﻿using System;
using DataFunc.Extensions.MediatR.Extensions;
using DataFunc.Extensions.MediatR.Interfaces;
using MediatR;

namespace DataFunc.Extensions.Tests.Fakes
{
    public class FakeCachableRequest : IRequest<Guid>, ICachable
    {
        public int FakePropertyOne => 1;
        public int FakePropertyTwo => 2;
        public int FakePropertyThree => 3;
        public string CacheKey => this.GenerateCacheKey(nameof(ICachable));
    }
}