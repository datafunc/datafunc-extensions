﻿using System;
using DataFunc.Extensions.MediatR.Extensions;
using DataFunc.Extensions.MediatR.Interfaces;

namespace DataFunc.Extensions.Tests.Fakes
{
    public class FakeClass: ICachable
    {
        public int FakePropertyOne => new Random().Next(0, int.MaxValue);
        public int FakePropertyTwo => new Random().Next(0, int.MaxValue);
        public int FakePropertyThree => new Random().Next(0, int.MaxValue);
        public string CacheKey => this.GenerateCacheKey("ahyeet");
    }
}