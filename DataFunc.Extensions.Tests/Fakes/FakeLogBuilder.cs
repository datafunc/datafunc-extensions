﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DataFunc.Extensions.Tests.Fakes
{
    public class FakeLogBuilder : ILoggingBuilder
    {
        public IServiceCollection Services { get; }
    }
}