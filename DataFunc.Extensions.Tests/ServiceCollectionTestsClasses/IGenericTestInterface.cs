﻿namespace DataFunc.Extensions.Tests.ServiceCollectionTestsClasses
{
    public interface IGenericTestInterface<T>
    {
        void Test(T test);
    }
}