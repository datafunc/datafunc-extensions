﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DataFunc.Extensions.MediatR.Behaviors;
using DataFunc.Extensions.Tests.Fakes;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;

namespace DataFunc.Extensions.Tests
{
    [TestFixture]
    public class CacheBehaviorTestFixture 
    {
        [Test]
        public async Task EnsureRequestIsReadFromCacheWhenAvailable()
        {
            IMemoryCache memoryCache = new MemoryCache(new MemoryCacheOptions());
            var request = new FakeCachableRequest();
            var cachedResponse = Guid.NewGuid();
            memoryCache.Set(request.CacheKey, cachedResponse);
            var behavior = new CachingBehavior<FakeCachableRequest,Guid>(new NullLogger<FakeCachableRequest>(), memoryCache);
            var result = await behavior.Handle(new FakeCachableRequest(), default, () => Task.FromResult(Guid.NewGuid()));

            Assert.AreEqual(result, cachedResponse);
        }

        [Test]
        public async Task EnsureResponseIsWrittenToCacheWhenNotAvailable()
        {
            IMemoryCache memoryCache = new MemoryCache(new MemoryCacheOptions());
            var request = new FakeCachableRequest();
       
            var behavior = new CachingBehavior<FakeCachableRequest, Guid>(new NullLogger<FakeCachableRequest>(), memoryCache);
            var result = await behavior.Handle(new FakeCachableRequest(), default, () => Task.FromResult(Guid.NewGuid()));

            Assert.AreEqual(memoryCache.Get<Guid>(request.CacheKey), result);
        }


        [Test]
        public async Task EnsureCacheItemIsDeletedAfterCommandIsHandled()
        {
            IMemoryCache memoryCache = new MemoryCache(new MemoryCacheOptions());
            var request = new FakeCachableRequest();
            var command = new FakeClearCacheCommand();

            memoryCache.Set(request.CacheKey, Guid.NewGuid());

            var behavior = new CachingBehavior<FakeClearCacheCommand, Unit>(new NullLogger<FakeClearCacheCommand>(), memoryCache);
            
            Assert.IsFalse(memoryCache.Get<Guid>(request.CacheKey) == Guid.Empty);
            await behavior.Handle(command, default, () => Task.FromResult(Unit.Value));
            Assert.IsTrue(memoryCache.Get<Guid>(request.CacheKey) == Guid.Empty);
        }

        [Test]
        public void EnsureGenerateCacheKeyIsSameOverInstances()
        {
            var firstFakeRequest = new FakeCachableRequest();
            var secondFakeRequest = new FakeCachableRequest();

            Assert.AreEqual(firstFakeRequest.CacheKey,secondFakeRequest.CacheKey);
        }

        [Test]
        public void EnsureGenerateCacheKeyIsSameOverInstancesWithoutProperties()
        {
            var firstFakeRequest = new FakeCachableRequestWithoutProperties();
            var secondFakeRequest = new FakeCachableRequestWithoutProperties();

            Assert.AreEqual(firstFakeRequest.CacheKey, secondFakeRequest.CacheKey);
        }


        [Test]
        public void EnsureGenerateCacheKeyIsNotSameOverDifferentTypes()
        {
            var firstFakeRequest = new FakeCachableRequestWithoutProperties();
            var secondFakeRequest = new SecondFakeCachableRequestWithoutProperties();

            Assert.AreNotEqual(firstFakeRequest.CacheKey, secondFakeRequest.CacheKey);
        }
    }
}