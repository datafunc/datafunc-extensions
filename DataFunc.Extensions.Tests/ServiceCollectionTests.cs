﻿using DataFunc.Extensions.Tests.ServiceCollectionTestsClasses;
using DataFunc.Extensions.Web;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace DataFunc.Extensions.Tests
{
    [TestFixture]
    public class ServiceCollectionTests
    {
        [Test]
        public void EnsureClassesGetAdded()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddClassesThatImplementInterface(typeof(ITestInterface), ServiceLifetime.Scoped, "DataFunc");

            var serviceProvider = serviceCollection.BuildServiceProvider();

            Assert.IsNotNull(serviceProvider.GetService(typeof(ITestInterface)));
        }

        [Test]
        public void EnsureGenericClassesGetAdded()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddClassesThatImplementInterface(typeof(IGenericTestInterface<>), ServiceLifetime.Scoped, "DataFunc");

            var serviceProvider = serviceCollection.BuildServiceProvider();

            Assert.IsNotNull(serviceProvider.GetService(typeof(IGenericTestInterface<TestGenericObject>)));
        }
    }
}