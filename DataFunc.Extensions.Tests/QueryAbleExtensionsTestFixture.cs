﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace DataFunc.Extensions.Tests
{
    [TestFixture]
    public class QueryAbleExtensionsTestFixture
    {
        [Test]
        public void EnsureSortingWorksOnParentItem()
        {
            var toTestItems = new[] { TestEnumerableItem.ForTest(1), TestEnumerableItem.ForTest(200), TestEnumerableItem.ForTest(2) }.AsQueryable();
            var sortedItems = toTestItems.OrderBy("Index", "desc").ToList();

            Assert.AreEqual(sortedItems[0].Index, 200);
        }

        [Test]
        public void EnsureSortingWorksOnChildItem()
        {
            var toTestItems = new[] { TestEnumerableItem.ForTest(1,"anthony"), TestEnumerableItem.ForTest(200,"bjondina"), TestEnumerableItem.ForTest(2,"nancy") }.AsQueryable();
            var sortedItems = toTestItems.OrderBy("Item.Name", "desc").ToList();

            Assert.AreEqual(sortedItems[0].Item.Name, "nancy");
        }

        [Test]
        public void EnsureNoExceptionIsThrownWithInvalidPropertyName()
        {
            var toTestItems = new[] { TestEnumerableItem.ForTest(1, "anthony"), TestEnumerableItem.ForTest(200, "bjondina"), TestEnumerableItem.ForTest(2, "nancy") }.AsQueryable();
            var sortedItems = toTestItems.OrderBy("Item.Names", "desc").ToList();

            Assert.AreEqual(sortedItems[0].Item.Name, "anthony");
        }
        [Test]
        public void EnsureNoExceptionIsThrownWithInvalidParentPropertyName()
        {
            var toTestItems = new[] { TestEnumerableItem.ForTest(1, "anthony"), TestEnumerableItem.ForTest(200, "bjondina"), TestEnumerableItem.ForTest(2, "nancy") }.AsQueryable();
            var sortedItems = toTestItems.OrderBy("Items.Names", "desc").ToList();

            Assert.AreEqual(sortedItems[0].Item.Name, "anthony");
        }


        [Test]
        public void EnsureIsSortedAscendingWithNoParameterSupplied()
        {
            var toTestItems = new[] { TestEnumerableItem.ForTest(1, "anthony"), TestEnumerableItem.ForTest(1, "aanthony"), TestEnumerableItem.ForTest(200, "bjondina"), TestEnumerableItem.ForTest(2, "nancy") }.AsQueryable();
            var sortedItems = toTestItems.OrderBy("Item.Name",null).ToList();

            Assert.AreEqual(sortedItems[0].Item.Name, "aanthony");
        }
    }

    public class TestEnumerableItem
    {
        public int Index { get; set; }
        public TestEnumableChildItem Item { get; set; }

        public static TestEnumerableItem ForTest(int index)
        {
            return new TestEnumerableItem { Index = index };
        }
        public static TestEnumerableItem ForTest(int index, string childName)
        {
            return new TestEnumerableItem { Index = index, Item =  TestEnumableChildItem.ForTest(childName)  };
        }
    }

    public class TestEnumableChildItem
    {
        public string Name { get; set; }
        public static TestEnumableChildItem ForTest(string name)
        {
            return new TestEnumableChildItem { Name = name };
        }
    }
}