using System;
using System.Linq;
using System.Threading.Tasks;
using DataFunc.Extensions.Logging;
using DataFunc.Extensions.Logging.Configuration;
using DataFunc.Extensions.MediatR.Behaviors;
using DataFunc.Extensions.Tests.Fakes;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;


namespace DataFunc.Extensions.Tests
{
    [TestFixture]
    public class LoggingTestFixture
    {
        private IWebHost _builder;

        [SetUp]
        public void Setup()
        {
            _builder = new WebHostBuilder()
                .ConfigureServices(x => x.UseDataFuncLogging(new DataFuncLoggingOptions { LogGroup = "my-unit-tests", LogStreamPrefix = "https://logging.datafunc.company:9200" }))
                .UseStartup<FakeStartup>()
                .Build();

        }

        [Test]
        public async Task EnsureSimpleLoggingCanBePerformed()
        {
            var logger = _builder.Services.GetService<ILogger<FakeClass>>();
            foreach (var iteration in Enumerable.Range(1, 1))
            {

                var kek = new FakeClass();

                logger.LogInformation($"Logging request {iteration} {JsonConvert.SerializeObject(kek)}");
                await Task.Delay(TimeSpan.FromMilliseconds(1));
            }
            await Task.Delay(TimeSpan.FromMilliseconds(5));
        }

        [Test]
        public void EnsureLogginBehaviourLogsTheRequest()
        {
            var logger = _builder.Services.GetService<ILogger<FakeLoggableRequest>>();
            var behavior = new LoggingBehavior<FakeLoggableRequest, Unit>(logger);

           Assert.DoesNotThrowAsync(async () => await behavior.Handle(new FakeLoggableRequest(), default, () => Task.FromResult(Unit.Value)));
           
        }

        [Test]
        public void EnsureLogginBehaviourLogsTheException()
        {
            var logger = _builder.Services.GetService<ILogger<FakeLoggableRequest>>();
            var behavior = new LoggingBehavior<FakeLoggableRequest, Unit>(logger);

            Assert.ThrowsAsync<NullReferenceException>(async () => await behavior.Handle(new FakeLoggableRequest(), default, () => Task.FromException<Unit>(new NullReferenceException("ah yeet"))));

        }

    }
}
