﻿
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;


namespace DataFunc.Services.Background
{
    public abstract class BackgroundService : IDisposable, IHostedService
    {
        protected readonly ILogger Logger;

        protected readonly CancellationTokenSource StoppingCts = new CancellationTokenSource();

        private Task _executingTask;
    

        protected BackgroundService(ILogger logger)
        {
            Logger = logger;
        }

        public virtual void Dispose()
        {
            StoppingCts.Cancel();
        }

        protected abstract Task ExecuteAsync(CancellationToken stoppingToken);

        public virtual Task StartAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation($"Starting task {this.GetType().Name}");
            // Store the task we're executing
            _executingTask = ExecuteAsync(StoppingCts.Token);

            // If the task is completed then return it,
            // this will bubble cancellation and failure to the caller
            if (_executingTask.IsCompleted)
            {
                return _executingTask;
            }

            // Otherwise it's running
            return Task.CompletedTask;
        }

        public virtual async Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation($"Stopping task {this.GetType().Name}");
            // Stop called without start
            if (_executingTask == null)
            {
                return;
            }

            try
            {
                // Signal cancellation to the executing method
                StoppingCts.Cancel();
            }
            finally
            {
                // Wait until the task completes or the stop token triggers
                await Task.WhenAny(_executingTask, Task.Delay(Timeout.Infinite, cancellationToken));
            }
        }
    }
}
