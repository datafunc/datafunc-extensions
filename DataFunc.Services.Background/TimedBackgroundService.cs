﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace DataFunc.Services.Background
{
    public abstract class TimedBackgroundService : BackgroundService
    {
        protected TimedBackgroundService(ILogger logger) : base(logger)
        {
        }

        public abstract TimeSpan DelayBetweenTicks { get; }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Logger.LogInformation($"Executing task {this.GetType().Name}");

            stoppingToken.Register(() => Logger.LogDebug("background task is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                await Process(stoppingToken);

                await Task.Delay(DelayBetweenTicks, stoppingToken);
            }

            Logger.LogInformation($"Ending execution of task {this.GetType().Name}");
        }

        protected abstract Task Process(CancellationToken canceltoken);

    }
}