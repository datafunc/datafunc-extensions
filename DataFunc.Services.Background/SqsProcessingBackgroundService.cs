﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.SQS;
using Amazon.SQS.Model;
using DataFunc.Sqs.Extensions;
using DataFunc.Sqs.Extensions.Message;
using Microsoft.Extensions.Logging;


namespace DataFunc.Services.Background
{
    public abstract class SqsProcessingBackgroundService<T> : TimedBackgroundService
    {
        protected IAmazonSQS QueueService { get; }
        protected abstract string QueueUrl { get; }
        protected abstract Task Process(QueueMessage<T> incomingMessage, CancellationToken token);


        private DateTime? _lastExecutionDate;
        private IEnumerable<QueueMessage<T>> _previousMessages;

        
        protected SqsProcessingBackgroundService(IAmazonSQS service, ILogger<T> logger) : base(logger)
        {
            QueueService = service;
        }

        public override TimeSpan DelayBetweenTicks => TimeSpan.FromSeconds(30);
        protected override async Task Process(CancellationToken canceltoken)
        {
            canceltoken.ThrowIfCancellationRequested();

            if (!ShouldRun(_lastExecutionDate, _previousMessages ?? Enumerable.Empty<QueueMessage<T>>()))
                return;

            var messageResponse = await QueueService.ReceiveMessages<T>(QueueUrl, WaitTimeSeconds, canceltoken);
            if (messageResponse.Any())
            {
                foreach (var queueMessage in messageResponse)
                {
                    canceltoken.ThrowIfCancellationRequested();
                    await Process(queueMessage, canceltoken);
                }

                canceltoken.ThrowIfCancellationRequested();

                var toDeleteMessages = messageResponse.Select(receivedMessage => new DeleteMessageBatchRequestEntry(receivedMessage.MessageId, receivedMessage.ReceiptHandle));
                await QueueService.DeleteMessageBatchAsync(new DeleteMessageBatchRequest(QueueUrl, toDeleteMessages.ToList()), canceltoken);
            }

            _previousMessages = messageResponse;
            _lastExecutionDate = DateTime.Now;
        }


        public virtual int WaitTimeSeconds => 20;
        public virtual int NumberOfMessagesToRetrievePerRun => 10;
        public virtual bool ShouldRun(DateTime? previousRun, IEnumerable<QueueMessage<T>> previousMessages) => true;

    }
}