﻿using System;
using System.Linq;
using System.Security.Claims;

namespace DataFunc.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static T ParseClaimValue<T>(this ClaimsPrincipal principal, string type,T defaultValue, Converter<string,T> converter)
        {
            var claim = principal.Claims.FirstOrDefault(x => x.Type.EqualsIgnoreCase(type));
            if (claim == null)
                return defaultValue;

            return converter.Invoke(claim.Value);
        }
    }
}