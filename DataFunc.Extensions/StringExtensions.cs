using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DataFunc.Extensions.Helpers;
using Newtonsoft.Json;

namespace DataFunc.Extensions
{
    public static class StringExtensions
    {
        public static string Mask(this string inputString, bool mask = true)
        {
            if (!mask || string.IsNullOrWhiteSpace(inputString)) return inputString;
            var rnd = new Random();
            var resultBuilder = new StringBuilder();
            var randomIndices = Enumerable.Range(0, inputString.Length)
                .OrderBy(x => rnd.Next())
                .Take(inputString.Length <= 3 ? 2 : inputString.Length / 3)
                .ToList();

            for (var i = 0; i < inputString.Length; i++)
            {
                resultBuilder.Append(randomIndices.Contains(i) ? '*' : inputString[i]);
            }
            return resultBuilder.ToString();

        }
        public static string RemoveSpaces(this string toTrimString)
        {
            return string.IsNullOrWhiteSpace(toTrimString) ? toTrimString : Regex.Replace(toTrimString, @"\s+", "");
        }
        public static string AsJson(this object toStringify)
        {
            return JsonConvert.SerializeObject(toStringify, Formatting.Indented);
        }
        public static string GetMimeType(string extension)
        {
            return MimeTypeMap.GetMimeType(extension.TrimStart('.').TrimEnd('.').Trim());
        }

        public static string CapitalizeFirstLetters(this string toCapitalizeString, string delimiter = " ")
        {
            if (string.IsNullOrWhiteSpace(toCapitalizeString))
                return toCapitalizeString;

            var toSplitContents = toCapitalizeString.Split(new[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < toSplitContents.Length; i++)
            {
                var splittedElement = toSplitContents[i];
                if (string.IsNullOrWhiteSpace(splittedElement))
                    continue;

                toSplitContents[i] = char.ToUpper(splittedElement[0]) + splittedElement.Substring(1).ToLower();
            }

            return string.Join(delimiter, toSplitContents);
        }

        public static string GetFileNameExtension(this string fileName)
        {
            return string.IsNullOrWhiteSpace(fileName) ?
                null :
                fileName.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
        }
        public static string GetExtension(string mimeType)
        {
            return MimeTypeMap.GetExtension(mimeType);
        }

        public static string ExtractLetters(this string @this)
        {
            return new string(@this.ToCharArray().Where(char.IsLetter).ToArray());
        }

        public static bool EqualsIgnoreCase(this string @this, string comparedString)
        {
            return @this.Equals(comparedString, StringComparison.OrdinalIgnoreCase);
        }

        public static string ExtractNumbers(this string @this)
        {
            return new string(@this.ToCharArray().Where(char.IsNumber).ToArray());
        }

        public static string RemoveNumbers(this string @this)
        {
            return new string((from x in @this.ToCharArray()
                               where !char.IsNumber(x)
                               select x).ToArray());
        }

        public static byte[] ToByteArray(this string @this)
        {
            return Activator.CreateInstance<ASCIIEncoding>().GetBytes(@this);
        }

        public static string RemoveLetters(this string @this)
        {
            return new string((from x in @this.ToCharArray()
                               where !char.IsLetter(x)
                               select x).ToArray());
        }

        public static bool IsValidTimeSpan(this string @this)
        {
            return TimeSpan.TryParse(@this, out _);
        }

        public static Stream ToMemoryStream(this string @this)
        {
            return new MemoryStream(Activator.CreateInstance<ASCIIEncoding>().GetBytes(@this));
        }

        public static string GetBetween(this string @this, string before, string after)
        {
            int num = @this.IndexOf(before);
            int num2 = num + before.Length;
            int num3 = @this.IndexOf(after, num2);
            if (num == -1 || num3 == -1)
            {
                return string.Empty;
            }
            return @this.Substring(num2, num3 - num2);
        }

        public static string Truncate(this string @this, int maxLength)
        {
            if (@this == null || @this.Length <= maxLength)
            {
                return @this;
            }
            int length = maxLength - "...".Length;
            return @this.Substring(0, length) + "...";
        }

        public static string Truncate(this string @this, int maxLength, string suffix)
        {
            if (@this == null || @this.Length <= maxLength)
            {
                return @this;
            }
            int length = maxLength - suffix.Length;
            return @this.Substring(0, length) + suffix;
        }
        public static string TruncateReverse(this string @this, int maxLength)
        {
            if (@this == null || @this.Length <= maxLength)
            {
                return @this;
            }
            int length = maxLength - "...".Length;
            return "..." + @this.Substring(@this.Length - length, length);
        }

        public static bool IsValidEmail(this string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            foreach (var splittedEmail in email.Split(';').Select(x => x.Trim()))
            {
                var toCheckEmail = splittedEmail;

                try
                {
                    // Normalize the domain
                    toCheckEmail = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                        RegexOptions.None, TimeSpan.FromMilliseconds(200));

                    // Examines the domain part of the email and normalizes it.
                    string DomainMapper(Match match)
                    {
                        // Use IdnMapping class to convert Unicode domain names.
                        var idn = new IdnMapping();

                        // Pull out and process domain name (throws ArgumentException on invalid)
                        var domainName = idn.GetAscii(match.Groups[2].Value);

                        return match.Groups[1].Value + domainName;
                    }

                    var response = Regex.IsMatch(toCheckEmail,
                           @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                           @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                           RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
                    if (!response)
                        return false;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return true;
        }
    }
}