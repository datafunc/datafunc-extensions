﻿using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Newtonsoft.Json;
using DataFunc.Exceptions;

namespace DataFunc.Extensions
{
    public static class LogExtensions
    {
        private static object ConvertForJson(object obj, DataFuncException ex = null, string note = "", [CallerMemberName] string memberName = "")
        {
            return new
            {
                Exception = ex?.Message,
                CustomMessage = note,
                MethodName = memberName,
                DateTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm"),
                ErrorCode = ex?.ErrorCode,
                ErrorMessage = ex?.ErrorMessage,
                StatusCode = ex?.StatusCode,
                Parameter = obj,
                AdditionalInformation = ex?.AdditionalInformation.Select(x =>
                    $"Key: {x.Key} - Value: {JsonConvert.SerializeObject(x.Value)}")
            };
        }
        public static void LogErrorAsJson(this ILogger log, object obj, Exception ex, string note = "", [CallerMemberName] string memberName = "")
        {

            if (ex is DataFuncException dfcException)
            {
                log.LogError(JsonConvert.SerializeObject(ConvertForJson(obj, dfcException, note, memberName), Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }));
            }
            else
            {
                log.LogError(JsonConvert.SerializeObject(ConvertForJson(obj, null, note, memberName), Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }));
            }
        }

        public static void LogWarningAsJson(this ILogger log, object obj, string note = "", [CallerMemberName] string memberName = "")
        {
            log.LogWarning(JsonConvert.SerializeObject(ConvertForJson(obj, null, note, memberName), Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            }));
        }

        public static void LogInformationAsJson(this ILogger log, object obj, string note = "", [CallerMemberName] string memberName = "")
        {
            log.LogInformation(JsonConvert.SerializeObject(ConvertForJson(obj, null, note, memberName), Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            }));
        }
    }
}