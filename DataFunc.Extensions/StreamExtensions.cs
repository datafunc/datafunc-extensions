﻿using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace DataFunc.Extensions
{
    public static class StreamExtensions
    {
        public static async Task<byte[]> ConvertAndDispose(this Stream stream)
        {
            using (var currentStream = stream)
            using (var memoryStream = new MemoryStream())
            {
                await currentStream.CopyToAsync(memoryStream);
                return memoryStream.ToArray();
            }
        }
    }
}