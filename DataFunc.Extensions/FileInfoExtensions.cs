using System;
using System.IO;
using System.Linq;

namespace DataFunc.Extensions
{
	public static class FileInfoExtensions
	{
		public static void Rename(this FileInfo @this, string newName)
		{
			string destFileName = Path.Combine(@this.Directory.FullName, newName);
			@this.MoveTo(destFileName);
		}

		public static string ChangeExtension(this FileInfo @this, string extension)
		{
			return Path.ChangeExtension(@this.FullName, extension);
		}

		public static string GetFileNameWithoutExtension(this FileInfo @this)
		{
			return Path.GetFileNameWithoutExtension(@this.FullName);
		}

		public static void Clear(this DirectoryInfo obj)
		{
			Array.ForEach(obj.GetFiles(), delegate(FileInfo x)
			{
				x.Delete();
			});
			Array.ForEach(obj.GetDirectories(), delegate(DirectoryInfo x)
			{
				x.Delete(recursive: true);
			});
		}

		public static long GetSize(this DirectoryInfo @this)
		{
			return @this.GetFiles("*.*", SearchOption.AllDirectories).Sum((FileInfo x) => x.Length);
		}
	}
}
