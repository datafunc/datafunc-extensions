﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace DataFunc.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddClassesThatImplementInterface(this IServiceCollection collection, Type interfaceType, ServiceLifetime lifeTime, string assembliesToScanPrefix = null)
        {
            if (!interfaceType.IsInterface)
                throw new Exception($"{interfaceType.Name} is not an interface");
            //Load Assemblies
            //Get All assemblies.
            var refAssembyNames = Assembly.GetExecutingAssembly()
                .GetReferencedAssemblies().ToList();

            refAssembyNames.Add(Assembly.GetCallingAssembly().GetName());
            //Load referenced assemblies
            foreach (var asslembyNames in refAssembyNames)
            {
                Assembly.Load(asslembyNames);
            }

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            if (!string.IsNullOrWhiteSpace(assembliesToScanPrefix))
                assemblies = assemblies.Where(x => x.FullName.StartsWith(assembliesToScanPrefix, true, CultureInfo.CurrentCulture)).ToArray();

            foreach (var assembly in assemblies)
            {
                //Find assemblies with your ServiceType
                var typesFromAssembly = assembly
                    .GetTypes()
                    .Where(p => p.GetInterface(interfaceType.Name) != null && !p.IsAbstract);


                foreach (var type in typesFromAssembly)
                {

                    if (interfaceType.ContainsGenericParameters)
                    {
                        var toAddInterface = type.GetInterfaces().Single(x => x.IsGenericType && x.GetGenericTypeDefinition() == interfaceType);

                        collection.Add(new ServiceDescriptor(toAddInterface, type, lifeTime));
                    }
                    else
                    {
                        collection.Add(new ServiceDescriptor(interfaceType, type, lifeTime));
                    }
                }
            }

            return collection;
        }
    }
}