using System;
using System.Globalization;

namespace DataFunc.Extensions
{
	public static class DateTimeExtensions
	{
		public static DateTime StartOfWeek(this DateTime dateTime)
		{
			int dayOfWeek = (int)dateTime.DayOfWeek;
			dayOfWeek = ((dayOfWeek == 0) ? 7 : dayOfWeek);
			return dateTime.AddDays(1 - dayOfWeek);
		}

		public static DateTime EndOfWeek(this DateTime @this)
		{
			return @this.StartOfWeek().AddDays(6.0);
		}

		public static DateTime StartOfMonth(this DateTime @this)
		{
			return new DateTime(@this.Year, @this.Month, 1);
		}

		public static DateTime EndOfMonth(this DateTime @this)
		{
			return DateTime.Now.StartOfMonth().AddDays(DateTime.DaysInMonth(@this.Year, @this.Month) - 1);
		}

		public static DateTime StartOfYear(this DateTime @this)
		{
			return new DateTime(@this.Year, 1, 1);
		}

		public static DateTime EndOfYear(this DateTime @this)
		{
			return new DateTime(@this.Year, 1, 1).AddYears(1).Subtract(new TimeSpan(0, 0, 0, 0, 1));
		}

        public static bool IsFuture(this DateTime date)
        {
            return DateTime.Now.Date < date.Date;
        }

		public static bool IsToday(this DateTime date)
        {
            return DateTime.Now.Date == date.Date;
        }

        public static bool IsYesterday(this DateTime date)
        {
            return date.Date == Yesterday(DateTime.Now.Date);
        }

		public static DateTime Tomorrow(this DateTime @this)
		{
			return @this.AddDays(1.0);
		}

		public static DateTime Yesterday(this DateTime @this)
		{
			return @this.AddDays(-1.0);
		}

		public static int Age(this DateTime @this)
		{
			if (DateTime.Today.Month < @this.Month || (DateTime.Today.Month == @this.Month && DateTime.Today.Day < @this.Day))
			{
				return DateTime.Today.Year - @this.Year - 1;
			}
			return DateTime.Today.Year - @this.Year;
		}

		public static int GetQuarter(this DateTime @this)
		{
			return Convert.ToInt16((@this.Month - 1) / 3) + 1;
		}

		public static double ToUnixTimeStamp(this DateTime dateTime)
		{
			return dateTime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
		}

		public static string GetMonthName(this DateTime date, CultureInfo culture)
		{
			return date.ToString("MMMM", culture);
		}

        public static string GetDutchRelativeDateDescription(this DateTime sourceDate)
        {
            const int second = 1;
            const int minute = 60 * second;
            const int hour = 60 * minute;
            const int day = 24 * hour;
            const int month = 30 * day;

            var ts = new TimeSpan(DateTime.Now.Ticks - sourceDate.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 1 * minute)
                return ts.Seconds == 1 ? "een seconde geleden" : ts.Seconds + " seconden geleden";

            if (delta < 2 * minute)
                return "een minuut geleden";

            if (delta < 45 * minute)
                return ts.Minutes + " minuten geleden";

            if (delta < 90 * minute)
                return "een uur geleden";

            if (delta < 24 * hour)
                return ts.Hours + " uren geleden";

            if (delta < 48 * hour)
                return "gisteren";

            if (delta < 30 * day)
                return ts.Days + " dagen geleden";

            if (delta < 12 * month)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "een maand geleden" : months + " maanden geleden";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return years <= 1 ? "een jaar geleden" : years + " jaren geleden";
            }
        }
	}
}
