﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DataFunc.Extensions
{
    public static class HttpClientExtensions
    {
        public static Task<HttpResponseMessage> PatchAsJsonAsync(this HttpClient client, string url, object toPostObject, CancellationToken token)
        {
            var message = new HttpRequestMessage(new HttpMethod("PATCH"), url)
            {
                Content = new StringContent(JsonConvert.SerializeObject(toPostObject), Encoding.UTF8, "application/json")
            };
            return client.SendAsync(message, token);
        }

        public static Task<HttpResponseMessage> PostAsJsonAsync(this HttpClient client, string url, object toPostObject, CancellationToken token)
        {
            var httpPostMessage = new StringContent(JsonConvert.SerializeObject(toPostObject), Encoding.UTF8, "application/json");
            return client.PostAsync(url, httpPostMessage, token);
        }

        public static Task<HttpResponseMessage> PutAsJsonAsync(this HttpClient client, string url, object toPostObject, CancellationToken token)
        {
            var httpPostMessage = new StringContent(JsonConvert.SerializeObject(toPostObject), Encoding.UTF8, "application/json");
            return client.PutAsync(url, httpPostMessage, token);
        }

        public static async Task<bool> IsJsonResponse(this HttpContent content)
        {
            var contents = await content.ReadAsStringAsync();
            if (string.IsNullOrWhiteSpace(contents))
                return false;

            try
            {
                var unused = JsonConvert.DeserializeObject(contents);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}