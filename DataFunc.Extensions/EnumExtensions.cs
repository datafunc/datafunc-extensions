using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace DataFunc.Extensions
{
	public static class EnumExtensions
	{
        public static List<T> GetEnumValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToList();
        }
        public static T GetValueFromDescription<T>(string description, T defaultValue)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();

            if (string.IsNullOrWhiteSpace(description))
                return defaultValue;

            foreach (var field in type.GetFields())
            {
                if (Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                {
                    if (attribute.Description.Equals(description, StringComparison.InvariantCultureIgnoreCase))
                        return (T) field.GetValue(null);
                }
                else
                {
                    if (field.Name.Equals(description, StringComparison.InvariantCultureIgnoreCase))
                        return (T) field.GetValue(null);
                }
            }

            return defaultValue;
        }
        public static string GetCustomAttributeDescription<T>(this Enum value,Func<T, string> retrievalFunction) where T : Attribute
        {
            return retrievalFunction(value.GetType().GetField(value.ToString()).GetCustomAttribute<T>(false));
        }
        public static string GetCustomAttributeDescription(this Enum value)
		{
			return (value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), inherit: false)
				.FirstOrDefault() as DescriptionAttribute)?.Description;
		}

		public static bool In(this Enum @this, params Enum[] values)
		{
			return Array.IndexOf(values, @this) != -1;
		}

		public static bool NotIn(this Enum @this, params Enum[] values)
		{
			return Array.IndexOf(values, @this) == -1;
		}
	}
}
