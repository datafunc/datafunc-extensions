﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataFunc.Extensions
{
    public static class ListExtensions
    {

        public static Dictionary<TKey, TValue> Merge<TKey,TValue>(this Dictionary<TKey, TValue> source, Dictionary<TKey, TValue> toMergeDictionary)
        {
            if (toMergeDictionary == null)
                return source;

            foreach (var keyValuePair in toMergeDictionary)
            {
                source[keyValuePair.Key] = keyValuePair.Value;
            }

            return source;
        }

        public static Dictionary<string,string> Merge(this Dictionary<string, string> source, params KeyValuePair<string, string>[] addons)
        {
            if (addons == null)
                return source;

            foreach (var keyValuePair in addons)
            {
                source[keyValuePair.Key] = keyValuePair.Value;
            }

            return source;
        }
        public static void AddOrUpdate<T>(this List<T> list, T item, Func<T, bool> existsOperator)
        {
            var existingItem = list.FirstOrDefault(existsOperator);
            if (existingItem != null)
                list.Remove(existingItem);

            list.Add(item);
        }
        public static string Join(this List<string> list, string seperator = ",")
        {
            return string.Join(seperator, list);
        }

        public static IEnumerable<int> SplitIntegers(this string list, char seperator = ',')
        {
            return list.Split(seperator).Select(int.Parse);
        }

        public static IEnumerable<Guid> SplitGuids(this string list, char seperator = ',')
        {
            return list.Split(seperator).Select(Guid.Parse);
        }
    }
}