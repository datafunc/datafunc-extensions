﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DataFunc.Extensions
{
    public static class QueryableExtensions
    {
        public static IOrderedQueryable<TSource> OrderBy<TSource>(this IQueryable<TSource> query, string propertyName, string sortDirection, char propertySplitter = '.') where TSource : class
        {
            string[] props = propertyName.Split(propertySplitter);
            Type type = typeof(TSource);
            ParameterExpression arg = Expression.Parameter(type, "x");
            Expression expr = arg;
            foreach (string prop in props)
            {
                // use reflection (not ComponentModel) to mirror LINQ
                PropertyInfo pi = type.GetProperties().FirstOrDefault(x => x.Name.Equals(prop, StringComparison.CurrentCultureIgnoreCase));
                if (pi == null)
                    return query.OrderBy(x => 0);

                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }
            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(TSource), type);
            LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);

            object result = typeof(Queryable).GetMethods().Single(
                    method => method.Name == ( sortDirection == "desc" ? "OrderByDescending" : "OrderBy")
                              && method.IsGenericMethodDefinition
                              && method.GetGenericArguments().Length ==
                              2
                              && method.GetParameters().Length == 2)
                .MakeGenericMethod(typeof(TSource), type)
                .Invoke(null, new object[] { query, lambda });
            return (IOrderedQueryable<TSource>)result;
        }

        public static IOrderedQueryable<TSource> ThenBy<TSource>(this IOrderedQueryable<TSource> query, string propertyName, string sortDirection, char propertySplitter = '.') where TSource : class
        {
            var props = propertyName.Split(propertySplitter);
            var type = typeof(TSource);
            var arg = Expression.Parameter(type, "x");
            Expression expr = arg;
            foreach (var prop in props)
            {
                // use reflection (not ComponentModel) to mirror LINQ
                var propertyInfo = type.GetProperties().FirstOrDefault(x => x.Name.Equals(prop, StringComparison.CurrentCultureIgnoreCase));
                if (propertyInfo == null)
                    continue;

                expr = Expression.Property(expr, propertyInfo);
                type = propertyInfo.PropertyType;
            }
            var delegateType =
                typeof(Func<,>).MakeGenericType(typeof(TSource), type);
            var lambda = Expression.Lambda(delegateType,
                expr, arg);

            var result = typeof(Queryable).GetMethods().Single(
                    method => method.Name == (sortDirection?.ToLower() == "desc" ? "ThenByDescending" : "ThenBy")
                              && method.IsGenericMethodDefinition
                              && method.GetGenericArguments().Length ==
                              2
                              && method.GetParameters().Length == 2)
                .MakeGenericMethod(typeof(TSource), type)
                .Invoke(null, new object[] { query, lambda });
            return (IOrderedQueryable<TSource>)result;
        }
    }
}