﻿using System;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DataFunc.Extensions.Web.Models
{
    internal class ExceptionOutputModel
    {
        public string DisplayUrl { get; set; }
        public string TraceIdentifier { get; set; }
        public string LocalIp { get; set; }
        public string RemoteIp { get; set; }
        public string RequestTime { get; set; }
        public Exception Exception { get; set; }

        public ExceptionOutputModel(ExceptionContext context)
        {
            DisplayUrl = context.HttpContext.Request.GetDisplayUrl();
            TraceIdentifier = context.HttpContext.TraceIdentifier;
            LocalIp = context.HttpContext.Connection.LocalIpAddress.ToString();
            RemoteIp = context.HttpContext.Connection.RemoteIpAddress.ToString();
            RequestTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            Exception = context.Exception;
        }
    }
}