﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using DataFunc.Exceptions;
using DataFunc.Extensions.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Newtonsoft.Json;

namespace DataFunc.Extensions.Web.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class DataFuncExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ILogger<DataFuncExceptionFilter> _logger;

        public DataFuncExceptionFilter(ILogger<DataFuncExceptionFilter> logger = null)
        {
            _logger = logger ?? NullLogger<DataFuncExceptionFilter>.Instance;
        }

        public override async Task OnExceptionAsync(ExceptionContext context)
        {
            var handledException = context.Exception as DataFuncException;

            var response = new
            {
                ErrorCode = handledException?.ErrorCode ?? "INTERNAL_SERVER_ERROR",
                ErrorMessage = handledException?.ErrorMessage ?? context.Exception.Message,
                StatusCode = handledException?.StatusCode ?? (int)HttpStatusCode.InternalServerError,
                AdditionalInformation = handledException?.AdditionalInformation ?? new Dictionary<string, object>
                {
                    { nameof(Exception.Message), context.Exception.Message },
                    { nameof(Exception.InnerException), context.Exception?.InnerException?.Message }
                }
            };

            if (handledException is null)
            {
                _logger.LogError(context.Exception, "Unhandled Exception on {displayUrl} method {method} query {query} error {error}",
                    context.HttpContext.Request.GetDisplayUrl(),
                    context.HttpContext.Request.Method,
                    context.HttpContext.Request.QueryString,
                    context.Exception.ToString());
            }

            context.Result = new JsonResult(response);

            context.HttpContext.Response.StatusCode = response.StatusCode;

            await base.OnExceptionAsync(context);
        }
    }
}