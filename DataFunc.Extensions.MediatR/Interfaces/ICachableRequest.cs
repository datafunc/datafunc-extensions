﻿using Newtonsoft.Json;

namespace DataFunc.Extensions.MediatR.Interfaces
{

    public interface ICachable
    {
        [JsonIgnore]
        string CacheKey { get; }
    }

    public interface ICanClearCache
    {
        [JsonIgnore]
        string CachePrefix { get; }
    }
}