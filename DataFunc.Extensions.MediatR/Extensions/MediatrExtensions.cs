﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace DataFunc.Extensions.MediatR.Extensions
{
    public static class MediatrExtensions
    {
        public static async Task<T> TrySend<T>(this IMediator mediatr, IRequest<T> request, T defaultValue, CancellationToken token)
        {
            try
            {
                return await mediatr.Send(request, token);
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}