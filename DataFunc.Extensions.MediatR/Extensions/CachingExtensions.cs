﻿using System;
using System.Text;
using DataFunc.Extensions.MediatR.Interfaces;
using Newtonsoft.Json;

namespace DataFunc.Extensions.MediatR.Extensions
{
    public static class CachingExtensions
    {
        public static string GenerateCacheKey(this ICachable cachable, string customPrefix)
        {
            if (string.IsNullOrWhiteSpace(customPrefix))
                return string.Empty;

            var toSerializeKey = JsonConvert.SerializeObject(cachable, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All});
            byte[] buffer = (new UTF8Encoding()).GetBytes(toSerializeKey);
            return $"{customPrefix}{Convert.ToBase64String(buffer)}";
        }
    }
}