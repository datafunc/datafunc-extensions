﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DataFunc.Extensions.MediatR.Interfaces;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace DataFunc.Extensions.MediatR.Behaviors
{
    public class CachingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger<TRequest> _logger;
        private readonly IMemoryCache _memoryCache;

        public CachingBehavior(ILogger<TRequest> logger, IMemoryCache memoryCache)
        {
            _logger = logger;
            _memoryCache = memoryCache;
        }
        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            if (request is ICachable cachableRequest)
                return await GetOrStoreInCache(cachableRequest, next);

            if (request is ICanClearCache clearCacheRequest)
                ClearCacheForKey(clearCacheRequest.CachePrefix);

            return await next();
        }

        private void ClearCacheForKey(string cachePrefix)
        {
            var cacheKey = OverrideCacheKey(cachePrefix);
            if (string.IsNullOrWhiteSpace(cacheKey))
                return;

            var toRemoveKeys = GetCacheKeys().Where(x => x.StartsWith(cacheKey));
            foreach (var removeKey in toRemoveKeys)
            {
                _memoryCache.Remove(removeKey);
                _logger.LogInformation($"Removed {removeKey} from cache");
            }
        }

        private async Task<TResponse> GetOrStoreInCache(ICachable request, RequestHandlerDelegate<TResponse> next)
        {
            var cacheKey = OverrideCacheKey(request.CacheKey);
            if (string.IsNullOrWhiteSpace(cacheKey))
                return await next();

            return await _memoryCache.GetOrCreateAsync(cacheKey, (x) =>
            {
                x.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5);
                return next();
            });
        }

        public virtual string OverrideCacheKey(string generatedCacheKey)
        {
            return generatedCacheKey;
        }

        public IEnumerable<string> GetCacheKeys()
        {

            var field = _memoryCache.GetType().GetProperty("EntriesCollection", BindingFlags.NonPublic | BindingFlags.Instance);
            if (field == null)
                yield break;

            if (!(field.GetValue(_memoryCache) is ICollection collection))
                yield break;

            PropertyInfo methodInfo = null;
            foreach (var item in collection)
            {

                if (methodInfo == null)
                    methodInfo = item.GetType().GetProperty("Key");

                if (methodInfo == null)
                    continue;

                var val = methodInfo.GetValue(item);
                yield return val.ToString();
            }
        }
    }
}
