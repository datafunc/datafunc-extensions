﻿

using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DataFunc.Extensions.MediatR.Behaviors
{
    public class PerformanceBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger<TRequest> _logger;

        public PerformanceBehavior(ILogger<TRequest> logger)
        {
            _logger = logger;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var sw = new Stopwatch();

            sw.Start();

            var response = await next();

            sw.Stop();

            if (sw.ElapsedMilliseconds > 300)
            {
                _logger.LogInformation("Received long running request that took longer than 300 ms ( actual processing time : {ElapsedMilliseconds} ms ) {RequestName} {RequestContents}",sw.ElapsedMilliseconds, typeof(TRequest).Name, request.AsJson());
            }

            return response;
        }
    }
}