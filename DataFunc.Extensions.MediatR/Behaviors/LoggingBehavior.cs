﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;


namespace DataFunc.Extensions.MediatR.Behaviors
{
    public class LoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger<TRequest> _logger;

        public LoggingBehavior(ILogger<TRequest> logger)
        {
            _logger = logger;
        }
        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            try
            {
               return await next();
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while handling request {RequestName} {RequestContents} {Exception}", typeof(TRequest).Name, request.AsJson().Truncate(5000),exception);
                throw;
            }
        }
    }
}