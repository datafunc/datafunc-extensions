﻿

// DataFunc.Authentication.Extensions.HttpContextExtensions

using System;
using System.Security.Claims;
using DataFunc.Authentication.Extensions.Models;
using Microsoft.AspNetCore.Http;

namespace DataFunc.Authentication.Extensions
{
    public static class HttpContextExtensions
    {
        public static TenantUser AsDataFuncTenantUser(this HttpContext context)
        {
            var tenantUser = new TenantUser
            {
                UserId = Convert.ToInt32(context.User.FindFirst("UserId")?.Value),
                Email = context.User.FindFirst(ClaimTypes.Email)?.Value,
                UserName = context.User.FindFirst(ClaimTypes.Name)?.Value
            };
            foreach (Claim claim in context.User.Claims)
            {
                if (tenantUser.Extensions.ContainsKey(claim.Type))
                    continue;

                tenantUser.Extensions.Add(claim.Type, claim.Value);
            }
            return tenantUser;
        }
    }
}
