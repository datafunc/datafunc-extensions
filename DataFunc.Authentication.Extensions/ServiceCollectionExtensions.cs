﻿

// DataFunc.Authentication.Extensions.ServiceCollectionExtensions

using System.Text;
using DataFunc.Authentication.Extensions.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace DataFunc.Authentication.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static AuthenticationBuilder AddDataFuncTenantAuthentication(this IServiceCollection services, string validIssuer, string validAudience, string securityKey)
        {
            return services
                .AddAuthentication("Bearer")
                .AddJwtBearer(options =>
                {

                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = true,
                        ValidIssuer = validIssuer,
                        ValidAudience = validAudience,
                        ValidateLifetime = true,
                        ValidateIssuer = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey))

                    };
                });
        }

        public static AuthenticationBuilder AddDataFuncTenantAuthentication(this IServiceCollection services, TenantAuthenticationConfiguration configuration)
        {
            return AddDataFuncTenantAuthentication(services, configuration.ValidIssuer, configuration.ValidAudience, configuration.SecurityKey);
        }
    }
}
