﻿

// DataFunc.Authentication.Models.TenantAuthenticationConfiguration
namespace DataFunc.Authentication.Extensions.Models
{
    public class TenantAuthenticationConfiguration
    {
        public string ValidIssuer
        {
            get;
            set;
        }

        public string ValidAudience
        {
            get;
            set;
        }

        public string SecurityKey
        {
            get;
            set;
        }
    }
}
