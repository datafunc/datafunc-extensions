﻿

// DataFunc.Authentication.Models.TenantUser

using System.Collections.Generic;

namespace DataFunc.Authentication.Extensions.Models
{
    public class TenantUser
    {

        public TenantUser()
        {
            Extensions = new Dictionary<string, string>();
        }

        public int UserId
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public Dictionary<string, string> Extensions
        {
            get;
            set;
        }
    }
}
