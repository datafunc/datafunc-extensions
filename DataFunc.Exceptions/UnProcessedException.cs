﻿using System.Net;

namespace DataFunc.Exceptions
{
    public class UnProcessedException : DataFuncException
    {
        public string Job;

        public UnProcessedException(string job)
        {
            Job = job;

            AdditionalInformation.Add(nameof(Job), job);
        }

        public override string ErrorCode => "STILL_PROCESSING";
        public override string ErrorMessage => $"Job {Job} is still processing";
        public override int StatusCode => (int)HttpStatusCode.Accepted;
    }
}