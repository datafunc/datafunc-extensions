﻿using System;
using System.Collections.Generic;

namespace DataFunc.Exceptions
{
    public abstract class DataFuncException : Exception
    {
        protected DataFuncException()
        {

        }
        protected DataFuncException(string error) : base(error)
        {

        }
        public abstract string ErrorCode { get; }
        public abstract string ErrorMessage { get; }
        public abstract int StatusCode { get; }
        public Dictionary<string, object> AdditionalInformation { get; } = new Dictionary<string, object>();
    }
}