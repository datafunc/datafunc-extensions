﻿using System;
using System.Net;

namespace DataFunc.Exceptions
{
    public class DuplicateEntryException : DataFuncException
    {
        private readonly string _propertyName;
        private readonly object _propertyValue;

        public DuplicateEntryException(string propertyName, object propertyValue)
        {
            _propertyName = propertyName;
            _propertyValue = propertyValue;

            AdditionalInformation.Add(nameof(propertyName), propertyName);
            AdditionalInformation.Add(nameof(propertyValue), propertyValue);
        }

        public override string ErrorCode => "DUPLICATE_ENTRY";
        public override string ErrorMessage => $"Er bestaat reeds een item met {_propertyName} ({_propertyValue})";
        public override int StatusCode => (int)HttpStatusCode.Conflict;
    }
}