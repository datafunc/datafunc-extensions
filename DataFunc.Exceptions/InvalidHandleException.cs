﻿using System;
using System.Net;

namespace DataFunc.Exceptions
{
    public class InvalidHandleException : DataFuncException
    {
        public InvalidHandleException(string message) : base(message)
        {
            ErrorMessage = message;
        }

        public override string ErrorCode => "INVALID_ACTION";
        public override string ErrorMessage { get; }
        public override int StatusCode => (int) HttpStatusCode.BadRequest;
    }
}