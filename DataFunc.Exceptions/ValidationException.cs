﻿using System;
using System.Collections.Generic;
using System.Net;

namespace DataFunc.Exceptions
{
    public sealed class ValidationException : DataFuncException
    {
        public List<string> Errors { get; }

        public ValidationException(string message, List<string> errors) : this(errors)
        {
            ErrorMessage = message;
        }

        public ValidationException(List<string> errors)
        {
            Errors = errors;
            AdditionalInformation.Add(nameof(errors), errors);
            ErrorMessage = ErrorCode;
        }

        public override string ErrorCode => "VALIDATION_ERROR";
        public override string ErrorMessage { get; }
        public override int StatusCode => (int)HttpStatusCode.BadRequest;
    }
}
