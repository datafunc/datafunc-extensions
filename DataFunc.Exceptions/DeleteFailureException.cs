﻿using System;
using System.Net;

namespace DataFunc.Exceptions
{
    public sealed class DeleteFailureException : DataFuncException
    {
        public DeleteFailureException(object key, string message)
        {
            AdditionalInformation.Add(nameof(key), key);
            AdditionalInformation.Add(nameof(message), message);
            ErrorMessage = message;
        }

        public override string ErrorCode => "DELETE_FAILURE";
        public override string ErrorMessage { get; }
        public override int StatusCode => (int)HttpStatusCode.Conflict;
    }
}
