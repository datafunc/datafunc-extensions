﻿using System;
using System.Net;

namespace DataFunc.Exceptions
{
    public class InvalidConfigurationException : DataFuncException
    {
        private readonly string _propertyName;

        public InvalidConfigurationException(string propertyName)
        {
            _propertyName = propertyName;

            AdditionalInformation.Add(nameof(propertyName), propertyName);
        }

        public override string ErrorCode => "INVALID_CONFIGURATION";
        public override string ErrorMessage => $"Waarde {_propertyName} ontbreekt in configuratiebestand";
        public override int StatusCode => (int) HttpStatusCode.InternalServerError;
    }
}