﻿using System;
using System.Net;

namespace DataFunc.Exceptions
{
    public class UnauthorizedException : DataFuncException
    {
        public override string ErrorCode => "UNAUTHORIZED";
        public override string ErrorMessage => "UNAUTHORIZED";
        public override int StatusCode => (int)HttpStatusCode.Unauthorized;
    }
}