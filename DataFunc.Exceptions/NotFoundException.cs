﻿using System;
using System.Linq;
using System.Net;

namespace DataFunc.Exceptions
{
    public class NotFoundException : DataFuncException
    {
        public string Entity;

        public object Key;

        public NotFoundException(string entity, object key)
        {
            Entity = entity;
            Key = key;

            AdditionalInformation.Add(nameof(entity), entity);
            AdditionalInformation.Add(nameof(key), key);
        }

        public override string ErrorCode => "NOT_FOUND";
        public override string ErrorMessage => $"Entity {Entity} with key {Key} not found";
        public override int StatusCode => (int)HttpStatusCode.NotFound;
    }
}